$(document).ready(function() {

	function centerImage() {

		var thumbContainer = $('.seller-thumb__img'),
			thumbWidth     = thumbContainer.width(),
			thumbHeight    = thumbContainer.height();
		
		thumbContainer.find('img').each(function() {

			if( $(this).width() > thumbWidth || $(this).height() > thumbHeight ) {

				if( $(this).width() > thumbWidth ) {

					var widthDifference = $(this).width() - thumbWidth,
						widthDivision	= widthDifference / 2;

					$(this).css({ position: 'relative', left: -widthDivision });
				}
				else {

					$(this).css({ top: '', left: '' });
				}
				
				if( $(this).height() > thumbHeight ) {

					var heightDifference = $(this).height() - thumbHeight,
						heightDivision 	 = heightDifference / 2;

					$(this).css({ position: 'relative', top: -heightDivision });
				}
				else {
					
					$(this).css({ top: '', left: '' });
				}
			}
		});
	}

	function centerImageTimeout() {
		
		setTimeout(function() {

			centerImage();
		}, 600);
	}

	function fullpageHero() {

		$('#hero').css({
			width: '100%',
			height: $(window).height(),
			display: 'table'
		});

		$('.hero-container').css({ display: 'table-cell', verticalAlign: 'middle' });
	}

	function setSellerThumbnail(eq) {

		var midThumb   = $('.seller-thumb:eq('+eq+')'),
			midAction  = midThumb.children('a'),
			midWrapper = midThumb.closest('.owl-wrapper');
			midWrapper.css('height', midThumb.height() - midAction.outerHeight());

		midThumb.parent().addClass('active-seller').css({
			height: midThumb.height(),
			marginTop: -(midAction.outerHeight() / 2)
		});
	}

	// FullPage Hero script
	if( $(window).width() > 768 ) {
		
		fullpageHero();
	}

	$(window).resize(function() {

		if( $(window).width() > 768 ) {

			fullpageHero();
		}
		else {

			$('#hero, .hero-container').removeAttr('style');
		}
	});

	// Hero action click behaviour
	$('.hero-video').fancybox({
		width       : 1280,
	    height      : 720,
		wrapCSS		: 'hero-video-wrapper',
		aspectRatio : true,
		padding		: 6,
		helpers 	: {
	        media: true
	    }
	});

	$('.hero-action').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing');
	});

	// Seller Carousel scripts
	var sellerCarousel = $('#seller-thumbnail');

	sellerCarousel.owlCarousel({
		items: 8,
		itemsCustom: [[0, 1], [401, 2], [600, 3], [769, 3], [1025, 5], [1441, 7]],
		afterInit: centerImage,
		afterInit: function() {

			var visibleLength = this.owl.visibleItems.length,
				midPoint;

			if( visibleLength == 7 ) {
				midPoint = 3;
			}
			else if ( visibleLength == 5 ) {
				midPoint = 2;
			}
			else if ( visibleLength == 3 ) {
				midPoint = 1;
			}
			else {
				midPoint = 0;
			}

			setSellerThumbnail(midPoint);
		},
		afterAction: function() {

			var visible       = this.owl.visibleItems,
				visibleLength = visible.length;

			if( visibleLength == 1 ) {

				setSellerThumbnail(visible);
			}
		},
		afterUpdate: function() {

			centerImageTimeout();
			sellerCarousel.find('.owl-wrapper').css('height', $('.seller-thumb__img').height());
		}
	});

	$('.seller-thumbs__nav.prev').click(function() { sellerCarousel.trigger('owl.prev'); });
	$('.seller-thumbs__nav.next').click(function() { sellerCarousel.trigger('owl.next'); });
	
	$('.seller-thumb').on('mouseenter', function() {

		var action  = $(this).children('a'),
			wrapper = $(this).closest('.owl-wrapper');

		if( !wrapper.hasClass('grabbing') ) {
			
			wrapper.css('height', $(this).height() - action.outerHeight());
			wrapper.find('.active-seller').removeClass('active-seller').css({
				height: '',
				marginTop: ''
			});

			$(this).parent().addClass('active-seller').css({
				height: $(this).height(),
				marginTop: -(action.outerHeight() / 2)
			});
		}
	});

	$('.modalTrigger').fancybox({
		wrapCSS: 'seller-modal',
		autoResize: true,
		helpers: {
			overlay: {
				locked: true
			}
		}
	});

	$('.seller-video').lazyYT({
		loading_text: null
	});

	var wow = new WOW({
		offset: 50,
		mobile: false
	});

	wow.init();
});






























































































// $('.slot__box img').load(function() {

// 	var defaultSlot = Math.floor((Math.random() * 4) + 1);

// 	var slot1 = $('#slot1').slotMachine({
// 		active: defaultSlot,
// 		delay: 500
// 	});

// 	var slot2 = $('#slot2').slotMachine({
// 		active: defaultSlot,
// 		delay: 700
// 	});
	
// 	var slot3 = $('#slot3').slotMachine({
// 		active: defaultSlot,
// 		delay: 899
// 	});
	
// 	$(window).load(function() {
		
// 		slot1.next();
// 		slot2.next();
// 		slot3.next();
// 	});
// });

// $(document).ready(function() {

// 	var tag = document.createElement('script');
// 	tag.src = "http://www.youtube.com/iframe_api";
// 	var firstScriptTag = document.getElementsByTagName('script')[0];
// 	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 	var player;

// 	onYouTubeIframeAPIReady = function () {
// 	    player = new YT.Player('player', {
// 	        width: '100%',
// 	        height: '100%',
// 	        videoId: 'AkyQgpqRyBY',  // youtube video id
// 	        playerVars: {
// 	            'autoplay': 0,
// 	            'rel': 0,
// 	            'showinfo': 0
// 	        },
// 	        events: {
// 	            'onStateChange': onPlayerStateChange
// 	        }
// 	    });
// 	};

// 	onPlayerStateChange = function (event) {
// 	    if (event.data == YT.PlayerState.ENDED) {
// 	        $('.start-video').fadeIn('normal');
// 	    }
// 	};

// 	$(document).on('click', '.start-video', function () {
// 	    $(this).fadeOut('normal');
// 	    player.playVideo();
// 	});
// });